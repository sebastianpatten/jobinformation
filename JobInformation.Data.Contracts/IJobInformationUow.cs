﻿namespace JobInformation.Data.Contracts
{
    public interface IJobInformationUow
    {
        void Commit();

        IJobRepository Jobs { get; }
    }
}