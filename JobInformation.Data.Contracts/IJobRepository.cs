﻿using System.Linq;

namespace JobInformation.Data.Contracts
{
    public interface IJobRepository
    {
        IQueryable<Job> GetAll();
        Job Get(short jobNumber, string branch, string systemOfReference);
        void Add(Job job);
        void Update(Job job);
    }
}