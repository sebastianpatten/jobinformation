﻿using System;

namespace JobInformation.Data
{
    public partial class Job
    {
        public Job(string techId, string jobType, short jobNumber, string status, string branch, string createdBy, DateTime createDate, string systemOfReference)
        {
            TechId = techId;
            JobType = jobType;
            JobNumber = jobNumber;
            Status = status;
            Branch = branch;
            CreatedBy = createdBy;
            CreateDate = createDate;
            SystemOfReference = systemOfReference;
        }

        public void AddJobNote(JobNote jobNote)
        {
            JobNotes.Add(jobNote);
        }
    }
}