﻿using System;

namespace JobInformation.Data
{
    public partial class JobNote
    {
        public JobNote()
        {
        }

        public JobNote(string note, string techId, DateTime? returnDateTime, bool attentionRequired, DateTime createDate, string createdBy, short jobNumber, string jobBranch, string jobSystemOfReference)
        {
            Note = note;
            TechId = techId;
            ReturnDateTime = returnDateTime;
            AttentionRequired = attentionRequired;
            CreateDate = createDate;
            CreatedBy = createdBy;
            JobNumber = jobNumber;
            JobBranch = jobBranch;
            JobSystemOfReference = jobSystemOfReference;
        }
    }
}