﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using JobInformation.Data.Contracts;

namespace JobInformation.Data
{
    public class JobInformationUow : IJobInformationUow, IDisposable
    {
        private DbContext dbContext;

        public JobInformationUow(DbContext dbContext, IJobRepository jobs)
        {
            this.dbContext = dbContext;
            Jobs = jobs;
        }

        public void Commit()
        {
            dbContext.SaveChanges();
        }

        public IJobRepository Jobs { get; private set; }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dbContext != null)
                {
                    dbContext.Dispose();
                }
            }
        }
    }

    public class JobInformationDbContext : DbContext
    {
        protected JobInformationDbContext() : base()
        {
            TurnOffFeaturesThatHamperSerialization();
        }

        protected JobInformationDbContext(DbCompiledModel model) : base(model)
        {
            TurnOffFeaturesThatHamperSerialization();
        }

        public JobInformationDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            TurnOffFeaturesThatHamperSerialization();
        }

        public JobInformationDbContext(string nameOrConnectionString, DbCompiledModel model) : base(nameOrConnectionString, model)
        {
            TurnOffFeaturesThatHamperSerialization();
        }

        public JobInformationDbContext(DbConnection existingConnection, bool contextOwnsConnection) : base(existingConnection, contextOwnsConnection)
        {
            TurnOffFeaturesThatHamperSerialization();
        }

        public JobInformationDbContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection) : base(existingConnection, model, contextOwnsConnection)
        {
            TurnOffFeaturesThatHamperSerialization();
        }

        public JobInformationDbContext(ObjectContext objectContext, bool dbContextOwnsObjectContext) : base(objectContext, dbContextOwnsObjectContext)
        {
            TurnOffFeaturesThatHamperSerialization();
        }

        private void TurnOffFeaturesThatHamperSerialization()
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;
        }
    }
}