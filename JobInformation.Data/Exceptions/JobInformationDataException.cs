﻿using System;
using System.Runtime.Serialization;

namespace JobInformation.Data.Exceptions
{
    public class JobInformationDataException : ApplicationException
    {
        public JobInformationDataException()
        {
        }

        public JobInformationDataException(string message) : base(message)
        {
        }

        public JobInformationDataException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected JobInformationDataException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}