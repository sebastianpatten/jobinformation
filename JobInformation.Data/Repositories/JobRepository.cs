﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using JobInformation.Data.Contracts;

namespace JobInformation.Data.Repositories
{
    public class JobRepository : EFRepository<Job>, IJobRepository
    {
        public JobRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public override Job GetById(int id)
        {
            throw new NotSupportedException("You cannot search for a Job by Id. Use Get(short jobNumber, string branch, string systemOfReference) instead.");
        }

        public IQueryable<Job> Get()
        {
            return DbSet;
        }
        
        public Job Get(short jobNumber, string branch, string systemOfReference)
        {
            try
            {
                return
                    Get()
                        .Include(x => x.JobNotes)
                        .First(
                            x =>
                                x.JobNumber == jobNumber && x.Branch == branch &&
                                x.SystemOfReference == systemOfReference);
            }
            catch (InvalidOperationException)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
        }
    }
}