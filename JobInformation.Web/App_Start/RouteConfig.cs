﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace JobInformation.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapHttpRoute("ControllerOnly", "v1/{controller}");
            routes.MapHttpRoute("ControllerWithParameters", "v1/{controller}/{id}");
            routes.MapHttpRoute("JobNotes", "v1/{controller}/{id}/JobNotes");
        }
    }
}
