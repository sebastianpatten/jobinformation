﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Http;
using JobInformation.Data;
using JobInformation.Data.Contracts;
using JobInformation.Data.Repositories;

namespace JobInformation.Web.Controllers
{
    public class JobsController : ApiController
    {
        // This would be DI Injected
        private DbContext dbContext;
        private IJobRepository jobRepository;
        private JobInformationUow unitOfWork;

        public JobsController()
        {
            dbContext = new JobInformationDbContext("FFM_JobInformationEntities");
            jobRepository = new JobRepository(dbContext);
            unitOfWork = new JobInformationUow(dbContext, jobRepository);
        }

        /// <summary>
        /// Get All Jobs
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Job> Get()
        {
            return unitOfWork.Jobs.GetAll();
        }

        /// <summary>
        /// Get Specific Job
        /// </summary>
        /// <param name="id"></param>
        /// <param name="branch"></param>
        /// <param name="systemOfReference"></param>
        /// <returns></returns>
        public Job Get(short id, string branch, string systemOfReference)
        {
            return unitOfWork.Jobs.Get(id, branch, systemOfReference);
        }

        /// <summary>
        /// Create a new Job
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public Job Post(Job job)
        {
            unitOfWork.Jobs.Add(job);
            unitOfWork.Commit();
            return job;
        }

        public JobNote Post(short id, string branch, string systemOfReference, JobNote jobNote)
        {
            var job = unitOfWork.Jobs.Get(id, branch, systemOfReference);
            job.AddJobNote(jobNote);
            unitOfWork.Jobs.Update(job);
            unitOfWork.Commit();
            return jobNote;
        }
    }
}
